<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DeckRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"Deck:read"}},
 *     denormalizationContext={"groups"={"Deck:write"}}
 * )
 * @ORM\Entity(repositoryClass=DeckRepository::class)
 */
class Deck
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"Deck:read","Deck:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"Deck:read","Deck:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $format;

    /**
     * @Groups({"Deck:read","Deck:write"})
     * @ORM\Column(type="string", length=255)
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="decks")
     */
    private $user;//nom de l'utilisateur

    /**
     * @Groups({"Deck:read","Deck:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @Groups({"Deck:read","Deck:write"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $theme;

    /**
     * @Groups({"Deck:read","Deck:write"})
     * @ORM\ManyToMany(targetEntity=Carte::class)
     */
    private $cards;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @return Collection|Carte[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Carte $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
        }

        return $this;
    }

    public function removeCard(Carte $card): self
    {
        $this->cards->removeElement($card);

        return $this;
    }
}
