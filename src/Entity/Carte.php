<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CarteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"Carte:read"}},
 *     denormalizationContext={"groups"={"Carte:write"}}
 * )
 * @ORM\Entity(repositoryClass=CarteRepository::class)
 */
class Carte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $cmc;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $colorIdentity;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $colors;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rule;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $keyword;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $types;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $subtypes;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $setName;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="cards")
     */
    private $user;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\ManyToMany(targetEntity=Deck::class)
     */
    private $deck;

    /**
     * @Groups({"Carte:read","Carte:write"})
     * @ORM\Column(type="string", length=255)
     */
    private $urlImage;

    public function __construct()
    {
        $this->uer = new ArrayCollection();
        $this->deck = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCmc(): ?string
    {
        return $this->cmc;
    }

    public function setCmc(string $cmc): self
    {
        $this->cmc = $cmc;

        return $this;
    }

    public function getColorIdentity(): ?string
    {
        return $this->colorIdentity;
    }

    public function setColorIdentity(string $colorIdentity): self
    {
        $this->colorIdentity = $colorIdentity;

        return $this;
    }

    public function getColors(): ?string
    {
        return $this->colors;
    }

    public function setColors(string $colors): self
    {
        $this->colors = $colors;

        return $this;
    }

    public function getRule(): ?string
    {
        return $this->rule;
    }

    public function setRule(?string $rule): self
    {
        $this->rule = $rule;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getTypes(): ?string
    {
        return $this->types;
    }

    public function setTypes(string $types): self
    {
        $this->types = $types;

        return $this;
    }

    public function getSubtypes(): ?string
    {
        return $this->subtypes;
    }

    public function setSubtypes(string $subtypes): self
    {
        $this->subtypes = $subtypes;

        return $this;
    }

    public function getSetName(): ?string
    {
        return $this->setName;
    }

    public function setSetName(string $setName): self
    {
        $this->setName = $setName;

        return $this;
    }

    /**
     * @return Collection|User2[]
     */
    public function getUer(): Collection
    {
        return $this->uer;
    }

    public function addUer(User2 $uer): self
    {
        if (!$this->uer->contains($uer)) {
            $this->uer[] = $uer;
        }

        return $this;
    }

    public function removeUer(User2 $uer): self
    {
        $this->uer->removeElement($uer);

        return $this;
    }

    /**
     * @return Collection|Deck[]
     */
    public function getDeck(): Collection
    {
        return $this->deck;
    }

    public function addDeck(Deck $deck): self
    {
        if (!$this->deck->contains($deck)) {
            $this->deck[] = $deck;
        }

        return $this;
    }

    public function removeDeck(Deck $deck): self
    {
        $this->deck->removeElement($deck);

        return $this;
    }

    public function getUrlImage(): ?string
    {
        return $this->urlImage;
    }

    public function setUrlImage(string $urlImage): self
    {
        $this->urlImage = $urlImage;

        return $this;
    }
}
