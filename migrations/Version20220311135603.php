<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311135603 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE carte (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, cmc VARCHAR(255) NOT NULL, color_identity VARCHAR(255) NOT NULL, colors VARCHAR(255) NOT NULL, rule VARCHAR(255) DEFAULT NULL, keyword VARCHAR(255) DEFAULT NULL, types VARCHAR(255) NOT NULL, subtypes VARCHAR(255) NOT NULL, set_name VARCHAR(255) NOT NULL, url_image VARCHAR(255) NOT NULL, INDEX IDX_BAD4FFFDA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE carte_deck (carte_id INT NOT NULL, deck_id INT NOT NULL, INDEX IDX_E8820FEFC9C7CEB6 (carte_id), INDEX IDX_E8820FEF111948DC (deck_id), PRIMARY KEY(carte_id, deck_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deck (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, format VARCHAR(255) NOT NULL, user VARCHAR(255) NOT NULL, color VARCHAR(255) NOT NULL, theme VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deck_carte (deck_id INT NOT NULL, carte_id INT NOT NULL, INDEX IDX_7B890512111948DC (deck_id), INDEX IDX_7B890512C9C7CEB6 (carte_id), PRIMARY KEY(deck_id, carte_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE carte ADD CONSTRAINT FK_BAD4FFFDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE carte_deck ADD CONSTRAINT FK_E8820FEFC9C7CEB6 FOREIGN KEY (carte_id) REFERENCES carte (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE carte_deck ADD CONSTRAINT FK_E8820FEF111948DC FOREIGN KEY (deck_id) REFERENCES deck (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE deck_carte ADD CONSTRAINT FK_7B890512111948DC FOREIGN KEY (deck_id) REFERENCES deck (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE deck_carte ADD CONSTRAINT FK_7B890512C9C7CEB6 FOREIGN KEY (carte_id) REFERENCES carte (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carte_deck DROP FOREIGN KEY FK_E8820FEFC9C7CEB6');
        $this->addSql('ALTER TABLE deck_carte DROP FOREIGN KEY FK_7B890512C9C7CEB6');
        $this->addSql('ALTER TABLE carte_deck DROP FOREIGN KEY FK_E8820FEF111948DC');
        $this->addSql('ALTER TABLE deck_carte DROP FOREIGN KEY FK_7B890512111948DC');
        $this->addSql('ALTER TABLE carte DROP FOREIGN KEY FK_BAD4FFFDA76ED395');
        $this->addSql('DROP TABLE carte');
        $this->addSql('DROP TABLE carte_deck');
        $this->addSql('DROP TABLE deck');
        $this->addSql('DROP TABLE deck_carte');
        $this->addSql('DROP TABLE user');
    }
}
