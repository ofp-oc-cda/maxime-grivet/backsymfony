<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311150447 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carte CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE cmc cmc VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE color_identity color_identity VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE colors colors VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE rule rule VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE keyword keyword VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE types types VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE subtypes subtypes VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE set_name set_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE url_image url_image VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE deck CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE format format VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE user user VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE color color VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE theme theme VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE mail mail VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
